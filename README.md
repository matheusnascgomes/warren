### Stack

- Angular 12 
- RxJS
- Jest

## Decisões de Arquitetura

### Paginas

- **dashboard:** Rota principal da aplicação e porta de entrada para o agrupamento de todos os componentes da aplicação e redirecionamento de rotas
- **transactions/:id** Exibição dos detalhes de uma transação

**Obs.: Pensando em otimizar a experiência do usuário, optou-se em abrir o modal de detalhes em uma rota, para que o usuário saiba onde está, em caso de refresh, ou possa salvar a URL e acessar em outro momento com facilidade.**

### Modulos

- Features: Módulo de centralização das funcionalidades/paginas da aplicação.
- Shared: Módulo de centralização de componentes, serviços, modelos, valores estáticos e pipes disponibilizados para toda a aplicação

### Componentes (modules)

- **header:** Embrulha a logo e demais informações eventualmente necessárias no cabeçalho;
- **logo:** Logo da empresa contendo imagem ou títulos;
- **body**: Embrulha os componentes principais da aplicação, assim como filtros e cards;
- **cards-wrapper:** Agrupa todos os itens da listagem
- **searchbar:** Barra de busca para pesquisa baseada em texto;
- **status-select:** Seleção de filtro baseado em opções predefinidas;
- **card-item:** Item individual e responsivo da listagem de lista de transações;
- **details-modal:** Modal de detalhes de cada transação;
- **status-progress:** Barra de progresso dos status contendo as opções "Solicitada", "Processando" e "Concluída"

### Pipes

- **first-letter:** Retorna a primeira letra de uma palavra em UPPER_CASE para exibir como "ícone" de cada card
- **money-formatter:** Forma o valor da API em um valor monetário Brasileiro
- **status-translation:** Traduz o valor dos status das transações para português

### Store

- **filters.service**: Estado global da aplicação para controle dos filtros aplicados em todos os componentes independentes de nível hierárquico
- transaction.service: Estado global da aplicação para controle dos dados exibidos na tabela para efeito instantâneo em caso de aplicações de filtros.

### Internacionalização

Pensando em facilitar uma possível internacionalização da aplicação, todo o conteúdo de texto em português foi isolado em um arquivo .ts, que é modular o suficiente adicionar um novo idioma sem grandes trabalhos.

### Abordagens e paradígmas

**Mobile First**

O desenvolvimento da aplicação foi orientado a Mobile First, ou seja, toda a interface foi pensada e desenvolvida inicialmente para uma experiência mobile.

**BEM CSS**

Todas as classes CSS (com SASS) criadas guiaram-se pela nomenclatura sugerida pelo padrão BEM (block, elements, modifies)

**SCAM (Single Module Component)**

A arquitetura da aplicação foi planejada e desenvolvida guiada pelo SCAM, onde todos os componentes, desde os mais micro, são módulos independentes, que facilitam sua portabilidade e otimiza o desempenho, dado que o browser processa/baixa apenas o que é necessário.

### Onde é possível melhorar?

- [ ]  Cobertura de testes unitários
- [ ]  Adicionar Testes E2E com Cypress
- [ ]  Responsividade em mais dispositivos
- [ ]  Feedbacks de erro
- [ ]  Suavidade das transições entre componentes
- [ ]  Loading de pesquisa
