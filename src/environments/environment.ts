export const environment = {
  production: false,
  baseUrl: 'https://warren-transactions-api.herokuapp.com/api',
};
