import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { first, switchMap, tap } from 'rxjs/operators';
import * as texts from '../../../../shared/static/text.pt-br';
import { TransactionsService } from '../../../../shared/services/transactions.service';
import { Transaction } from '../../../../shared/models/transactions.model';

@Component({
  selector: 'app-transactions.model',
  templateUrl: './transaction.component.html',
  styleUrls: ['./transaction.component.scss'],
})
export class TransactionComponent implements OnInit {
  readonly texts = texts;

  transaction: Partial<Transaction> | undefined;

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private transactionService: TransactionsService,
  ) {}

  ngOnInit(): void {
    this.activatedRoute.params
      .pipe(
        first(), // Executa o subscribe apenas uma vez (envitando memory leak).
        switchMap(({ id }) => this.transactionService.transactionDetails(id)),
        tap(transaction => {
          const { title, from, to, amount, status } = transaction;
          this.transaction = {
            title,
            from,
            to,
            amount,
            status,
          };
        }),
      )
      .subscribe();
  }

  closeModal($event: boolean) {
    if ($event) {
      this.router.navigate(['']).then(_ => {});
    }
  }
}
