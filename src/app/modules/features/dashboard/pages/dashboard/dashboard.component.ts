import { Component, OnInit } from '@angular/core';
import { TransactionsService } from '../../../../shared/services/transactions.service';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {
  constructor(private transactionService: TransactionsService) {}

  ngOnInit(): void {
    this.transactionService.fetchTransactions().pipe(first()).subscribe();
  }
}
