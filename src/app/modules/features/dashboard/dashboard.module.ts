import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { TransactionComponent } from './pages/transaction/transaction.component';
import { HeaderModule } from '../../shared/components/header/header.module';
import { BodyModule } from '../../shared/components/body/body.module';
import { DetailsModalModule } from '../../shared/components/details-modal/details-modal.module';
import { StatusProgressModule } from '../../shared/components/status-progress/status-progress.module';
import { MoneyFormatterModule } from '../../shared/pipes/money-formatter/money-formatter.module';

@NgModule({
  declarations: [DashboardComponent, TransactionComponent],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    HeaderModule,
    BodyModule,
    DetailsModalModule,
    StatusProgressModule,
    MoneyFormatterModule,
  ],
})
export class DashboardModule {}
