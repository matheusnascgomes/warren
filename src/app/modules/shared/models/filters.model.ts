import { TransactionStatus } from './transactions.model';

/**
 * Modelo de dados dos filtros.
 */
export interface Filters {
  searchTerm?: String;
  options?: TransactionStatus;
}

/**
 * Modelo de dados das opções.
 */
export interface StatusOptions {
  title: string;
  value: TransactionStatus;
}
