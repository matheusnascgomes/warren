/**
 * Modelo de dados de uma transação do sistema.
 */
export interface Transaction {
  id: string;
  title: string;
  description: string;
  status: TransactionStatus;
  amount: number;
  date: string;
  from: string;
  to: string;
}

/**
 * Opções de status de uma transação.
 */
export const enum TransactionStatus {
  processing = 'processing',
  processed = 'processed',
  created = 'created',
}

/**
 * Valor para representar cada etapa de uma transação.
 */
export enum TransactionStages {
  todo = 'todo',
  current = 'current',
  done = 'done',
}
