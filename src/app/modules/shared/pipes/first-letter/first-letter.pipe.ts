import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'firstLetter',
})
export class FirstLetterPipe implements PipeTransform {
  transform(word: string | undefined): string {
    if (!word) return '';
    return word.charAt(0).toLocaleUpperCase();
  }
}
