import { FirstLetterPipe } from './first-letter.pipe';

describe('FirstLetterPipe', () => {
  it('Should deal with undefined value', () => {
    const pipe = new FirstLetterPipe();
    expect(pipe.transform(undefined)).toBe('');
  });

  it('Should return the first letter of a word in Uppercase', () => {
    const pipe = new FirstLetterPipe();
    expect(pipe.transform('warren')).toBe('W');
  });
});
