import { Pipe, PipeTransform } from '@angular/core';
import { TransactionStatus } from '../../models/transactions.model';
import * as texts from '../../static/text.pt-br';

@Pipe({
  name: 'statusTranslate',
})
export class StatusTranslatePipe implements PipeTransform {
  transform(status: TransactionStatus | undefined): string {
    if (!status) return '';

    switch (status) {
      case TransactionStatus.processed:
        return texts.statusOptionProcessed;
      case TransactionStatus.processing:
        return texts.statusOptionProcessing;
      case TransactionStatus.created:
        return texts.statusOptionCreated;
      default:
        throw 'Unmapped status';
    }
  }
}
