import { StatusTranslatePipe } from './status-translate.pipe';
import * as texts from '../../static/text.pt-br';

import { TransactionStatus } from '../../models/transactions.model';

describe('StatusTranslatePipe', () => {
  it(`Should translate status ${TransactionStatus.processed} properly`, () => {
    const pipe = new StatusTranslatePipe();
    expect(pipe.transform(TransactionStatus.processed)).toBe(
      texts.statusOptionProcessed,
    );
  });

  it(`Should translate status ${TransactionStatus.processing} properly`, () => {
    const pipe = new StatusTranslatePipe();
    expect(pipe.transform(TransactionStatus.processing)).toBe(
      texts.statusOptionProcessing,
    );
  });

  it(`Should translate status ${TransactionStatus.created} properly`, () => {
    const pipe = new StatusTranslatePipe();
    expect(pipe.transform(TransactionStatus.created)).toBe(
      texts.statusOptionCreated,
    );
  });
});
