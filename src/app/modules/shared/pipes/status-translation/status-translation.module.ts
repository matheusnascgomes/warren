import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StatusTranslatePipe } from './status-translate.pipe';

@NgModule({
  declarations: [StatusTranslatePipe],
  imports: [CommonModule],
  exports: [StatusTranslatePipe],
})
export class StatusTranslationModule {}
