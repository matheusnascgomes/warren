import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MoneyFormatterPipe } from './money-formatter.pipe';

@NgModule({
  declarations: [MoneyFormatterPipe],
  imports: [CommonModule],
  exports: [MoneyFormatterPipe],
})
export class MoneyFormatterModule {}
