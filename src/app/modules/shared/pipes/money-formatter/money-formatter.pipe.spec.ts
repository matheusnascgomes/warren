import { MoneyFormatterPipe } from './money-formatter.pipe';

describe('MoneyFormatterPipe', () => {
  it('Should deal with undefined values', () => {
    const pipe = new MoneyFormatterPipe();
    expect(pipe.transform(undefined)).toBe('');
  });

  it('Should format money properly', () => {
    const pipe = new MoneyFormatterPipe();
    expect(pipe.transform(5.55)).toBe('R$ 5,55'); // R$ 5,55
  });
});
