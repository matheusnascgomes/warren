import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'moneyFormatter',
})
export class MoneyFormatterPipe implements PipeTransform {
  transform(value: number | undefined): string {
    if (!value) return '';

    return value.toLocaleString('pt-BR', {
      style: 'currency',
      currency: 'BRL',
    });
  }
}
