import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Transaction } from '../../models/transactions.model';

@Component({
  selector: 'app-card-item',
  templateUrl: './card-item.component.html',
  styleUrls: ['./card-item.component.scss'],
})
export class CardItemComponent implements OnInit {
  @Input() transaction: Transaction | undefined;

  constructor(private route: Router) {}

  ngOnInit(): void {}

  handleCardClick() {
    this.route.navigate(['transaction', this.transaction?.id]).then(_ => {});
  }
}
