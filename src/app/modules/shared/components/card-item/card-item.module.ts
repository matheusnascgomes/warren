import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardItemComponent } from './card-item.component';
import { MoneyFormatterModule } from '../../pipes/money-formatter/money-formatter.module';
import { StatusTranslationModule } from '../../pipes/status-translation/status-translation.module';
import { FirstLetterModule } from '../../pipes/first-letter/first-letter.module';

@NgModule({
  declarations: [CardItemComponent],
  imports: [
    CommonModule,
    MoneyFormatterModule,
    StatusTranslationModule,
    FirstLetterModule,
  ],
  exports: [CardItemComponent],
})
export class CardItemModule {}
