import { Component, OnDestroy, OnInit } from '@angular/core';
import * as texts from '../../static/text.pt-br';
import { FormControl } from '@angular/forms';
import {
  debounceTime,
  distinctUntilChanged,
  map,
  takeUntil,
} from 'rxjs/operators';
import { FiltersService } from '../../services/filters.service';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-searchbar',
  templateUrl: './searchbar.component.html',
  styleUrls: ['./searchbar.component.scss'],
})
export class SearchbarComponent implements OnInit, OnDestroy {
  readonly texts = texts;

  searchTerm = new FormControl();

  destroy$ = new Subject();

  constructor(private filtersSevrice: FiltersService) {}

  ngOnInit(): void {
    this.searchTerm.valueChanges
      .pipe(
        takeUntil(this.destroy$), // Permite encerrar o subscribe quando o componete for destruído.
        debounceTime(400), // Impede novas chamadas em um intervalo de 400s.
        distinctUntilChanged(), // Impede novas chamadas para mesmo termo digitado em um intervalo.
        map(searchTerm => {
          this.filtersSevrice.updateFilters({ searchTerm });
        }),
      )
      .subscribe();
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
