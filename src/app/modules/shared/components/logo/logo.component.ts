import { Component, OnInit } from '@angular/core';

import * as texts from '../../static/text.pt-br';

@Component({
  selector: 'app-logo',
  template: `<img src="assets/logo.png" [alt]="texts.LogoALT" />`,
})
export class LogoComponent implements OnInit {
  readonly texts = texts;

  constructor() {}

  ngOnInit(): void {}
}
