import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StatusSelectComponent } from './status-select.component';

@NgModule({
  declarations: [StatusSelectComponent],
  imports: [CommonModule],
  exports: [StatusSelectComponent],
})
export class StatusSelectModule {}
