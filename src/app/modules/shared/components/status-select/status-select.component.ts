import { Component, OnInit } from '@angular/core';
import { statusOptions } from '../../static/options';
import { FiltersService } from '../../services/filters.service';
import { StatusOptions } from '../../models/filters.model';

@Component({
  selector: 'app-status-select',
  templateUrl: './status-select.component.html',
  styleUrls: ['./status-select.component.scss'],
})
export class StatusSelectComponent implements OnInit {
  placeholder = 'Status';

  options: StatusOptions[] = statusOptions;

  selectedItem: StatusOptions | undefined;

  opened = false;

  constructor(private filtersService: FiltersService) {}

  ngOnInit(): void {}

  handleChange(status: StatusOptions): void {
    this.selectedItem = statusOptions.find(i => i.value === status.value);
    this.toggle();
    this.filtersService.updateFilters({ options: this.selectedItem?.value });
  }

  toggle(): void {
    this.opened = !this.opened;
  }

  clearSearch(): void {
    this.selectedItem = undefined;
    this.filtersService.updateFilters({ options: undefined });
    this.toggle();
  }
}
