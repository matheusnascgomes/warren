import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  HostListener,
} from '@angular/core';

@Component({
  selector: 'app-details-modal',
  templateUrl: './details-modal.component.html',
  styleUrls: ['./details-modal.component.scss'],
})
export class DetailsModalComponent implements OnInit {
  readonly modalWrapperId = 'modalWrapperIdentifier';

  @HostListener('document:click', ['$event'])
  clickout(event: { target: HTMLElement }) {
    if (event.target.id === this.modalWrapperId) {
      this.closeAction();
    }
  }

  @Output() close = new EventEmitter<boolean>();

  constructor() {}

  ngOnInit(): void {}

  closeAction() {
    this.close.next(true);
    this.close.complete();
  }
}
