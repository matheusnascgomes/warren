import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardsWrapperComponent } from './cards-wrapper.component';
import { CardItemModule } from '../card-item/card-item.module';

@NgModule({
  declarations: [CardsWrapperComponent],
  imports: [CommonModule, CardItemModule],
  exports: [CardsWrapperComponent],
})
export class CardsWrapperModule {}
