import { Component, OnInit } from '@angular/core';
import { TransactionsService } from '../../services/transactions.service';

@Component({
  selector: 'app-cards-wrapper',
  templateUrl: './cards-wrapper.component.html',
  styleUrls: ['./cards-wrapper.component.scss'],
})
export class CardsWrapperComponent implements OnInit {
  constructor(public transactionService: TransactionsService) {}

  ngOnInit(): void {}
}
