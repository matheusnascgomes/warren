import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StatusProgressComponent } from './status-progress.component';



@NgModule({
  declarations: [
    StatusProgressComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    StatusProgressComponent
  ]
})
export class StatusProgressModule { }
