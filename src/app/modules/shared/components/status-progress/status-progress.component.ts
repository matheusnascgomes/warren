import {
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';
import * as texts from '../../static/text.pt-br';
import { TransactionStatus } from '../../models/transactions.model';
import { TransactionStages } from '../../models/transactions.model';

@Component({
  selector: 'app-status-progress',
  templateUrl: './status-progress.component.html',
  styleUrls: ['./status-progress.component.scss'],
})
export class StatusProgressComponent implements OnInit, OnChanges {
  @Input() status: TransactionStatus | undefined;

  stages: typeof TransactionStages = TransactionStages;

  firstStage: TransactionStages | undefined;
  secondStage: TransactionStages | undefined;
  thirdStage: TransactionStages | undefined;

  readonly texts = texts;

  constructor() {}

  ngOnInit(): void {}

  ngOnChanges(changes: SimpleChanges): void {
    const status = changes.status.currentValue as TransactionStatus;
    if (!status) {
      return;
    }
    switch (status) {
      case TransactionStatus.created:
        this.firstStage = TransactionStages.current;
        this.secondStage = TransactionStages.todo;
        this.thirdStage = TransactionStages.todo;
        break;
      case TransactionStatus.processing:
        this.firstStage = TransactionStages.done;
        this.secondStage = TransactionStages.current;
        this.thirdStage = TransactionStages.todo;
        break;
      case TransactionStatus.processed:
        this.firstStage = TransactionStages.done;
        this.secondStage = TransactionStages.done;
        this.thirdStage = TransactionStages.current;
        break;
      default:
        throw 'Unmapped status';
    }
  }
}
