import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BodyComponent } from './body.component';
import { CardsWrapperModule } from '../cards-wrapper/cards-wrapper.module';
import { SearchbarModule } from '../searchbar/searchbar.module';
import { StatusSelectModule } from '../status-select/status-select.module';

@NgModule({
  declarations: [BodyComponent],
  imports: [
    CommonModule,
    CardsWrapperModule,
    SearchbarModule,
    StatusSelectModule,
  ],
  exports: [BodyComponent],
})
export class BodyModule {}
