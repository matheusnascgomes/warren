import { Observable, BehaviorSubject } from 'rxjs';

/**
 * Store global com operações básicas de gerenciamento de estado usando RxJS.
 */
export abstract class Store<T> {
  /**
   * Stado global de tipo genérico.
   * @private
   */
  private state$ = new BehaviorSubject<T | undefined>(undefined);

  /**
   * Busca o valor atual do estado.
   */
  getAll = (): T | undefined => this.state$.getValue();

  /**
   * Retorna um obserável do estado global.
   */
  getAll$ = (): Observable<T | undefined> => this.state$.asObservable();

  /**
   * Atualiza o valor do estado.
   * @param nextState
   */
  store = (nextState: T | undefined) => this.state$.next(nextState);

  /**
   * Possibilita o cancelamento da inscrição.
   */
  unsubscribe = () => this.state$.unsubscribe();
}
