import { Injectable } from '@angular/core';
import { Store } from './store';
import { Filters } from '../models/filters.model';
import { TransactionsService } from './transactions.service';
import { switchMap } from 'rxjs/operators';

/**
 * Serviço responsável por centrar as operações de filtros, delegando as atualizações
 * para novas chamadas a API.
 */
@Injectable({
  providedIn: 'root',
})
export class FiltersService extends Store<Filters> {
  constructor(private transactionService: TransactionsService) {
    super();
    this.getAll$()
      .pipe(
        switchMap(filters =>
          this.transactionService.fetchTransactions(filters),
        ),
      )
      .subscribe();
  }

  /**
   * Atualiza os filtros de forma incremental.
   * @param filters Novo valor do filtro.
   */
  updateFilters(filters: Filters): void {
    const newFilters = { ...this.getAll(), ...filters };
    this.store(newFilters);
  }
}
