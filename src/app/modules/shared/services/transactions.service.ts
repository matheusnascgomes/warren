import { Injectable } from '@angular/core';

import { environment } from '../../../../environments/environment';
import { Observable } from 'rxjs';
import { Transaction } from '../models/transactions.model';
import { HttpClient } from '@angular/common/http';
import { Store } from './store';
import { tap } from 'rxjs/operators';
import { Filters } from '../models/filters.model';

/**
 * Serviço responsável por lídar com as operações de consulta na API de transações.
 */
@Injectable({
  providedIn: 'root',
})
export class TransactionsService extends Store<Transaction[]> {
  /** Url base da API */
  readonly baseUrl = environment.baseUrl;

  constructor(private http: HttpClient) {
    super();
  }

  /**
   * Realiza a busca das transações (com ou sem filtros).
   * @param filters Filtros opcionais.
   */
  fetchTransactions(filters?: Filters): Observable<Transaction[]> {
    let filterTerm = filters ? TransactionsService.bindFilters(filters) : '';

    return this.http
      .get<Transaction[]>(`${this.baseUrl}/transactions${filterTerm}`)
      .pipe(tap(values => this.store(values)));
  }

  /**
   * Busca os detalhes de um item da API.
   * @param id
   */
  transactionDetails(id: string): Observable<Transaction> {
    return this.http.get<Transaction>(`${this.baseUrl}/transactions/${id}`);
  }

  /**
   * Monta a querystring dos filtros.
   * @param filters Opções de filtros.
   * @private
   */
  private static bindFilters(filters: Filters): string {
    const { searchTerm, options } = filters;

    if (searchTerm && options) {
      return `?title=${searchTerm}&status=${options}`;
    } else if (searchTerm) {
      return `?title=${searchTerm}`;
    } else if (options) {
      return `?status=${options}`;
    } else {
      return '';
    }
  }
}
