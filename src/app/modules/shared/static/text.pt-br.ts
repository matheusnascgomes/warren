export const LogoALT = 'Logo da Aplicação';
export const SearchbarPlaceholder = 'Pesquise pelo título';

// Options
export const statusOptionProcessing = 'Processando';
export const statusOptionProcessed = 'Processado';
export const statusOptionCreated = 'Criado';

export const transactionRescue = 'Resgate';
export const transactionDeposit = 'Depósito';
export const transactionInternalMovement = 'Movimentação Interna';
export const transactionSummaryFrom = 'Transferido De';
export const transactionSummaryTo = 'Para';

export const LoadingText = 'Carregando...';
