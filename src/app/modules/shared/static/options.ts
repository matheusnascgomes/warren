import { TransactionStatus } from '../models/transactions.model';
import * as texts from './text.pt-br';
import { StatusOptions } from '../models/filters.model';

export const statusOptions: StatusOptions[] = [
  {
    title: texts.statusOptionProcessing,
    value: TransactionStatus.processing,
  },
  {
    title: texts.statusOptionProcessed,
    value: TransactionStatus.processed,
  },
  {
    title: texts.statusOptionCreated,
    value: TransactionStatus.created,
  },
];
